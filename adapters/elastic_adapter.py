from elasticsearch import helpers, Elasticsearch
import pprint


class ElasticAdapter:
    es = Elasticsearch()

    @classmethod
    def create_index(cls, index_name, index_mapping):
        cls.es.indices.create(index=index_name, body=index_mapping, ignore=400)

    @classmethod
    def list_indices(cls):
        """list all the indices with their mappings"""
        pp = pprint.PrettyPrinter(indent=4)
        for index in cls.es.indices.get_alias("*"):
            print(index)
            pp.pprint(cls.es.indices.get_mapping(index=index))

    @classmethod
    def bulk_index(cls, index_name, doc_type, documents):
        helpers.bulk(cls.es, documents, index=index_name, doc_type=doc_type)

    @classmethod
    def index(cls, index_name, doc_type, document_id, document):
        cls.es.index(index_name, doc_type, document, id=document_id)

    @classmethod
    def search(cls, index_name, doc_type, query):
        return cls.es.search(index_name, doc_type, body=query)

    @classmethod
    def scan(cls,  index_name, doc_type, query):
        return helpers.scan(cls.es, index=index_name, doc_type=doc_type, query=query)

