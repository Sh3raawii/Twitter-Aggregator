import pika
import json
import threading


def start_consumer(current_app):
    exchange_name = "users"
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=current_app.config.get("MESSAGE_BROKER_URL")))
    channel = connection.channel()
    channel.exchange_declare(exchange=exchange_name, exchange_type='fanout')
    result = channel.queue_declare(exclusive=True)
    queue_name = result.method.queue
    channel.queue_bind(exchange=exchange_name, queue=queue_name)
    channel.basic_consume(callback, queue=queue_name, no_ack=True)
    thread = threading.Thread(target=channel.start_consuming, daemon=True)
    thread.start()


def callback(ch, method, properties, body):
    body = json.loads(body)
    from twitter_aggregator.tasks import calc_user_stats
    calc_user_stats.apply_async(args=[body])
