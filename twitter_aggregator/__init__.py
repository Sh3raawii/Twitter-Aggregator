from flask import Flask
from config import get_app_config
from broker import start_consumer
from .views import aggregator
from indexer import doc_indexer


def create_app(app_config=None, celery=False):
    app = Flask(__name__)

    # setting app config
    app_config = app_config if app_config else get_app_config()
    app.config.from_object(app_config)

    # register blueprints
    app.register_blueprint(aggregator)
    app.register_blueprint(doc_indexer)

    # start consumer
    if not celery:
        start_consumer(app)

    return app
