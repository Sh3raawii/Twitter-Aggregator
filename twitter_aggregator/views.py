from flask import Blueprint, jsonify
from indexer import retrieve_stats_for_user

aggregator = Blueprint('aggregator', __name__)


@aggregator.route('/<username>', methods=['GET'])
def get_user_stats(username):
    stats = retrieve_stats_for_user(username)
    if stats:
        return jsonify(stats), 200
    else:
        return '', 202
