import operator
from . import create_app
from .celery import create_celery
from config import get_app_config
from indexer import get_all_user_tweets, index_user_stats

celery = create_celery(create_app(get_app_config(), celery=True))


@celery.task
def calc_user_stats(user):
    """fetch all tweets from elastic search and calculate the user statistics"""
    hours = [0] * 24
    days = [0] * 7
    day_names = ("sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday")
    retweets = 0
    for tweet in get_all_user_tweets(user_id=user.get("user_id")):
        hour_index = tweet['created_at'].hour
        day_index = tweet['created_at'].weekday()
        days[day_index] += 1
        hours[hour_index] += 1
        retweets += tweet['retweets']
    user['retweets'] = retweets
    max_day_index, _ = max(enumerate(days), key=operator.itemgetter(1))
    user['peak_hour'], _ = max(enumerate(hours), key=operator.itemgetter(1))
    user['peak_day'] = day_names[max_day_index]
    index_user_stats(user)
    return "done for user: {}".format(user.get("screen_name"))
