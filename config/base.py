import os


class BaseConfig:
    ENV = "development"
    DEBUG = True
    BASE_DIR = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
    SECRET_KEY = "development_key"

    # Dummy Twitter Developer APP Credentials
    TWITTER_CONSUMER_KEY = ""
    TWITTER_CONSUMER_SECRET = ""
    TWITTER_ACCESS_TOKEN_KEY = ""
    TWITTER_ACCESS_TOKEN_SECRET = ""

    # Celery Configurations
    CELERY_BROKER_URL = "amqp://wuzzuf1:password@localhost/app1"
    CELERY_RESULT_BACKEND = "amqp"

    # Message Broker Configurations
    MESSAGE_BROKER_URL = "localhost"
