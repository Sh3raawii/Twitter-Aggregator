import datetime
from flask import Blueprint
from adapters.elastic_adapter import ElasticAdapter

doc_indexer = Blueprint('doc_indexer', __name__)

user_tweets_index_name = "user_tweets"
user_tweets_doc_name = "tweet"
user_stats_index_name = "user_stats"
user_stats_doc_name = "stats"
user_stats_index_mapping = {
    "mappings": {
        "stats": {
            "properties": {
                "screen_name": {
                    "type": "keyword"
                },
                "user_id": {
                    "type": "keyword"
                },
                "tweets": {
                    "type": "long"
                },
                "following": {
                    "type": "long"
                },
                "followers": {
                    "type": "long"
                },
                "likes": {
                    "type": "long"
                },
                "retweets": {
                    "type": "long"
                },
                "peak_day": {
                    "type": "keyword"
                },
                "peak_hour": {
                    "type": "byte"
                }
            }
        }
    }
}


@doc_indexer.before_app_first_request
def create_all_indices():
    ElasticAdapter.create_index(user_stats_index_name, user_stats_index_mapping)


def index_users_stats(users_stats):
    users_stats = set_doc_id(users_stats, "user_id")
    ElasticAdapter.bulk_index(user_stats_index_name, user_stats_doc_name, users_stats)


def index_user_stats(user_stats):
    document_id = user_stats.get("user_id")
    ElasticAdapter.index(user_stats_index_name, user_stats_doc_name, document_id, user_stats)


def retrieve_stats_for_user(screen_name):
    results = ElasticAdapter.search(user_stats_index_name, user_stats_doc_name,
                                    query={"query": {"term": {"screen_name": screen_name}}})
    results = results['hits']['hits']
    user_stats = None
    if len(results) > 0:
        user_stats = results[0]['_source']
        user_stats.pop("screen_name")
        user_stats.pop("user_id")
        user_stats.pop("likes")
    return user_stats


def get_all_user_tweets(user_id):
    for tweet in ElasticAdapter.scan(user_tweets_index_name, user_tweets_doc_name,
                                     query={"query": {"term": {"user_id": user_id}}}):
        # convert created_at from str to python datetime
        tweet = tweet["_source"]
        tweet['created_at'] = datetime.datetime.strptime(tweet['created_at'], "%Y-%m-%dT%H:%M:%S")
        yield tweet


def set_doc_id(doc_reader, id_field_name):
    """
    set the _id field to avoid duplicate documents
    :param doc_reader: list/generator of documents
    :param id_field_name: field name with unique value
    :return: doc
    """
    for doc in doc_reader:
        doc['_id'] = doc.get(id_field_name)
        yield doc
